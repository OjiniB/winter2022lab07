public class Board
{
    private Die die1;
    private Die die2;
    private int[][] dieRolls;
	private final int NUM_ROLLED_TIMES;

    public Board()
    {
        this.die1 = new Die();
        this.die2 = new Die();
        this.dieRolls = new int[6][6];
		this.NUM_ROLLED_TIMES = 3;
    }


    public boolean playATurn()
    {
        int dieFirst= die1.roll();
        int dieSecond = die2.roll();
        System.out.println("= = = = = = = = = = = = = = = = = = = = =\n");
        System.out.println("After the shut: \n");
        System.out.println(">>> ^ First DIE: " + dieFirst + " , ^ Second DIE: " + dieSecond);
		this.dieRolls[this.die1.getPips() - 1][this.die2.getPips() - 1 ] += 1;
		
        if (this.dieRolls[this.die1.getPips() - 1][this.die2.getPips() - 1] == NUM_ROLLED_TIMES)
        {
			System.out.println();
			System.out.println("Unfortunately , YOU LOST ");
            System.out.println("Those pair of dices are rolled 2 times before, this is the third and the last");
            System.out.println("<<<<< GAME OVER >>>>>");
            return true;
        }
        else
        {
			return false;
        }

    }
	
	public String toString()
    {
		String playing= "\n";
		for (int i = 0; i < dieRolls.length; i++)
		{
			
			for (int j = 0; j < dieRolls[i].length; j++)
			{
				
				playing += "[" + dieRolls[i][j] + "] ";
				}
				playing+= " " + " ( "+  (i + 1) + " )" + "\n" + "  " + "\n";
				} 

        return "first Die " + this.die1 +
                ", Second Die " + this.die2 + "\n" + 
		"\n-- Your Numbers are --\n{\n " + "\n(1) (2) (3) (4) (5) (6)" + "\n\n" + playing + "}\n\n" +
		"| first one " + this.die1 +
                ", Second one " + this.die2 + " |\n" 
           ;
    }

}
